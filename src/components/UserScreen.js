import React from 'react'
import metric from '../assets/metricremove.png'
import LogoutButton from './LogoutButton'
import Profile from './Profile';

const UserScreen = () => {
  return (
    <div className="bg-[#ecfeff] h-screen w-screen py-10 px-10">
      <div className="flex flex-row justify-between">
        <img className="" src={metric}  alt="logo"  />
        <h1 className='text-black
    font-poppins font-bold 
    items-center p-12'><Profile/></h1>
        <LogoutButton className = "" />
      </div>
    </div>

  )

}


export default UserScreen