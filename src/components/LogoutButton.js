import {useAuth0} from '@auth0/auth0-react';

import React from 'react'
import { Navigate } from 'react-router-dom';

const LogoutButton = () => {
    const {logout, isAuthenticated} = useAuth0();

  return (
    isAuthenticated && (
    <button  className="text-black
    font-poppins font-bold text[16px]
    py-3 px-8 rounded-full hidden sm:block" onClick={() => logout() && Navigate("/")}>
      Log Out
    </button>)
  )
}

export default LogoutButton