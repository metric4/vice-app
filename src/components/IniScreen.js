import React from 'react'
import metric from '../assets/metricremove.png'
import LoginButton from './LoginButton'
import { useAuth0 } from '@auth0/auth0-react'
import { useNavigate } from 'react-router-dom'

const IniScreen = () => {
  const { isLoading, error, isAuthenticated} = useAuth0();
  const navigate = useNavigate();
  return (

    <div className="grid h-screen place-items-center py-6 px-10"> 

      <img src={metric}  alt="logo"  />

      <button className="bg-bermuda text-black
        font-poppins font-bold text[16px]
        py-3 px-8 rounded-full hidden sm:block">
        {error && <p>Authentication Error</p>}
        {!error && isLoading && <p>Loading...</p>}  
        {!error && !isLoading && <LoginButton/>}
        {isAuthenticated && navigate("/user")}
      </button>
    </div>
  )
}

export default IniScreen