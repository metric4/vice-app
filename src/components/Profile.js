import {useAuth0} from '@auth0/auth0-react';
import icon from '../assets/icon.jpg'

import React from 'react'

const Profile = () => {
    const {user, isAuthenticated} = useAuth0();
    var name = user.name;
    var picture = icon;
    if(picture == null){
      picture = icon;
    }

  return (
    isAuthenticated && (
    <div className='flex flex-row justify-between'>
        <p>{name}</p>
    </div>
    )
  )
}

export default Profile