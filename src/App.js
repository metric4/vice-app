import {Route,Routes} from "react-router-dom";
import IniScreen from "./components/IniScreen";
import {useAuth0} from "@auth0/auth0-react";
// eslint-disable-next-line no-unused-vars
import styles from "./index.css"
import UserScreen from "./components/UserScreen";

function App(){
  const {isAuthenticated} = useAuth0();

  return (
    <Routes>
      <Route path="/" element={<IniScreen/>} />
      {isAuthenticated && <Route path="/user" element={<UserScreen/>}/>}
      <Route path="/*" element={<IniScreen/>} />

    </Routes>
  )
}
export default App;